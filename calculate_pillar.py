import sys
import json
from collections import OrderedDict

h = 0.35
w = 0.35

def make_ordered_point(x, y):
    point = OrderedDict()
    point["x"] = x
    point["y"] = y
    return point


def calculate_pillar(point, direct='l'):
    x = point[0]
    y = point[1]
    if direct == 'l':
        p0 = make_ordered_point( x, y-h)
        p1 = make_ordered_point(x,  y)
        p2 = make_ordered_point(x - w,  y)
        p3 = make_ordered_point(x - w,  y-h)
        res = [p0, p1, p2, p3]
    else:
        p0 = make_ordered_point(x+w, y-h)
        p1 = make_ordered_point(x+w, y)
        p2 = make_ordered_point(x, y)
        p3 = make_ordered_point(x, y-h)
        res = [p0, p1, p2, p3]
    print(json.dumps(res))

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python calculate_pillar.py {direction} {2d_point}")
        print('''Example: python calculate_pillar.py l "0.9,0.9"''')
        exit(-1)
    
    direct = sys.argv[1]
    point_str = sys.argv[2]
    point = eval("("+ point_str +")")
    calculate_pillar(point, direct)